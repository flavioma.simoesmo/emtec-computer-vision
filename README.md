# Computer Vision

By Cardoso Rafael and Moreira Flávio

## Getting started

### What is Computer Vision

Computer vision is a field of artificial intelligence and computer science that focuses on enabling computers to interpret and understand visual information from the real world. It involves developing algorithms and techniques to analyze, process, and extract meaningful insights from images or videos. Applications of computer vision range from image recognition and object detection to image segmentation and scene understanding.

### Technology advancements

The growth of computer vision technology over the past years has been remarkable, driven by advancements in artificial intelligence, deep learning, and the availability of large datasets.

These are the key developments:

- Deep Learning revolution: Deep learning techniques, particularly convolutional neural networks (CNNs), have revolutionized computer vision by enabling models to learn complex features directly from raw pixel data. This has significantly improved the accuracy of tasks such as image classification, object detection, and semantic segmentation.

- Availability of datasets: The availability of large, labeled datasets such as ImageNet, COCO, and OpenImages has played a crucial role in training and benchmarking computer vision models. These datasets have facilitated the development of more robust and generalized algorithms.

- Hardware improvements: Advances in hardware, including GPUs and specialized accelerators like TPUs, have accelerated the training and deployment of deep learning models for computer vision tasks. This has made it feasible to run complex models efficiently, even in resource-constrained environments.

- Applications across different industries: Computer vision technology has found applications across a wide range of industries, including healthcare (medical imaging, disease diagnosis), automotive (autonomous vehicles, driver assistance systems), retail (cashier-less stores, inventory management), agriculture (crop monitoring, yield prediction), and security (surveillance, facial recognition).

## Proof of Concept

### Objective

With this project, the goal was to develop a simple application that could receive an image with different shapes, analyze it and output the image with the name of the different shapes present in it.

The goal of this proof of concept is to be our first approach to the technology, help us be able to better understand what it's capable of, the current level of the technology and the future opportunities.

### Technologies used

- **Flutter** : 
Flutter is an open-source UI software development toolkit from Google, allowing developers to build natively compiled applications for mobile, web, and desktop from a single codebase.

- **FastAPI** : FastAPI is a modern, fast (high-performance), web framework for building APIs with Python 3.6+ based on standard Python type hints, making it easy to create robust and scalable API applications.

- **OpenCV** : OpenCV (Open Source Computer Vision Library) is an open-source computer vision and machine learning software library. It provides a comprehensive suite of functions and algorithms for image processing, computer vision tasks, and machine learning applications.

- **Railway.app** : Railway.app is a platform that simplifies the deployment and management of web applications by providing seamless integration with popular cloud providers, enabling developers to focus on building their applications without worrying about infrastructure management.

### Architecture

We have a Flutter application in this GitLab repository, it's a simple application that enables the user to choose an image from is gallery or to take a new one with the camera.

And we have a FastAPI application stored in the cloud with the Python code that does the Computer Vision analysis.

## Setup

### Python Script with FastAPI on Railway.app

Our Python script utilizes a FastAPI framework to create a RESTful API for image processing. When deployed on Railway.app, it enables seamless access to the image analysis functionality from anywhere on the web.

**Image Analysis with Python Script:**

The Python script with FastAPI uses the power of OpenCV to analyze uploaded images and detect various shapes within them.

**Image Upload and Decoding**:

- When a user uploads an image to the /upload/ endpoint, the script reads the contents of the uploaded image file and decodes it into a NumPy array using OpenCV's cv2.imdecode() function. This array represents the image in a format that can be processed and analyzed.

**Image Preprocessing**: 

- The script performs preprocessing steps to enhance the quality of the uploaded image and facilitate shape detection. This includes converting the image to grayscale (cv2.cvtColor()), applying thresholding to segment the image into binary regions (cv2.threshold()), and finding contours to identify potential shapes (cv2.findContours()).

**Shape Detection:**

- Using the contours extracted from the pre-processed image, the script iterates through each contour and applies the Douglas-Peucker algorithm (cv2.approxPolyDP()) to approximate the shape of the contour. This algorithm simplifies the contour while preserving its essential shape characteristics.

- Based on the number of vertices in the approximated shape, the script classifies it into one of several predefined shape categories, such as triangles, quadrilaterals, pentagons, hexagons, or circles. This classification is determined by the number of vertices returned by the approximation algorithm.

**Shape Annotation:**

- After classifying each shape, the script annotates the original image with text labels indicating the detected shapes and their corresponding categories. This annotation is performed using OpenCV's cv2.putText() function, which overlays text onto the image at specified coordinates.

**Image Encoding and Response Generation**:

- Finally, the script encodes the annotated image into JPEG format using OpenCV's cv2.imencode() function and returns it as a streaming response with the appropriate media type (image/jpeg) using FastAPI's StreamingResponse.

**Role of FastAPI:**

**Declarative syntax**: 

- FastAPI uses a declarative syntax that allows developers to define API endpoints, request parameters, and response models using standard Python type annotations.

**Automatic documentation**:

- FastAPI automatically generates interactive API documentation (Swagger UI) based on the defined endpoints, request parameters, and response models. 

**Data validation**:

- FastAPI performs automatic data validation and serialization of request payloads, ensuring that incoming data adheres to specified data types and constraints.

**Asynchronous support**:

- FastAPI fully supports asynchronous programming, allowing handlers to be defined as asynchronous functions (async def) for improved performance and scalability. Asynchronous handling enables concurrent execution of I/O-bound tasks, such as reading and processing image files.

**Middleware support**:

- FastAPI provides middleware support for extending and customizing the behavior of the application. In our code, a CORS (Cross-Origin Resource Sharing) middleware is added to allow cross-origin requests, enabling clients from different domains to access the API.

    - **app.add_middleware**: This method adds middleware to the FastAPI application. Middleware functions intercept HTTP requests and responses, allowing developers to modify or augment them before they reach the endpoint handler functions.

    - **CORSMiddleware**: This middleware specifically handles CORS requests. CORS is a security feature implemented by web browsers that restricts cross-origin HTTP requests initiated by client-side scripts. It ensures that resources on one origin (e.g., domain) are prevented from making requests to another origin unless explicit permission is granted.

    - **allow_origins=["*"]**: This parameter specifies which origins are allowed to access the resources of the FastAPI application. In this case, "*" indicates that all origins are allowed, meaning that requests from any domain are permitted. Alternatively, we could specify specific origins by providing a list of URLs.

    - **allow_credentials=True**: This parameter determines whether the client is allowed to include credentials (e.g., cookies, authorization headers) in cross-origin requests. Setting it to True indicates that credentials are allowed, while False would disallow them.

    - **allow_methods=["*"]**: This parameter specifies which HTTP methods are allowed for cross-origin requests. "*" indicates that all methods (GET, POST, PUT, DELETE, etc.) are permitted. It's possible to specify specific methods by providing a list of HTTP method names.

    - **allow_headers=["*"]**: This parameter specifies which HTTP headers are allowed in cross-origin requests. "*" indicates that all headers are permitted.

**Deployment on Railway.app:**

Railway.app simplifies the deployment and management of web applications, providing developers with a seamless and efficient platform for hosting their projects.

- The FastAPI-based Python script is deployed on Railway.app, which simplifies the deployment and management of web applications.

- Railway.app seamlessly integrates with popular cloud providers, ensuring reliable and scalable deployment of the FastAPI application.

This is the full python script:

```python
from fastapi import FastAPI, UploadFile
import cv2
import numpy as np
from starlette.responses import StreamingResponse
import io
from fastapi.middleware.cors import CORSMiddleware
from sympy import Idx

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.post("/upload/")
async def upload_image(file: UploadFile):
    # Lecture de l'image
    contents = await file.read()
    nparr = np.frombuffer(contents, np.uint8)
    image = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    _, thresh_image = cv2.threshold(gray_image, 220, 255, cv2.THRESH_BINARY)

    contours, hierarchy = cv2.findContours(thresh_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    unique_labels = set()

    for i, contour in enumerate(contours):
        if i == 0:
            continue

        epsilon = 0.01 * cv2.arcLength(contour, True)
        approx = cv2.approxPolyDP(contour, epsilon, True)

        # Extract bounding box coordinates
        x, y, w, h = cv2.boundingRect(approx)
        x_mid = int(x + (w / 3))
        y_mid = int(y + (h / 1.5))
        coords = (x_mid, y_mid)


        # Determine shape label based on the number of vertices
        if len(approx) == 3:
            shape_label = "Triangle"
        elif len(approx) == 4:
            shape_label = "Quadrilateral"
        elif len(approx) == 5:
            shape_label = "Pentagon"
        elif len(approx) == 6:
            shape_label = "Hexagon"
        else:
            shape_label = "Circle"

        # Draw contour and add shape label to the set
        cv2.drawContours(image, [contour], 0, (0, 0, 0), 4)
        cv2.putText(image, shape_label, coords, cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 0), 1)
        unique_labels.add(shape_label)

    # Draw unique shape labels on the image
    for label in unique_labels:
        cv2.putText(image, label, (10, 50 + 30 * Idx), cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 0), 1)

    _, img_encoded = cv2.imencode(".jpg", image)
    return StreamingResponse(io.BytesIO(img_encoded.tobytes()), media_type="image/jpeg")
```

**Example Output**:

For example, if the uploaded image contains triangles, quadrilaterals, and circles, the script will annotate the image with text labels indicating the detected shapes at their respective locations. The annotated image will then be returned as the output of the API endpoint, ready for display or further processing by the client application.

### Flutter application

Our simple Flutter application provides a user-friendly interface for selecting images from the gallery or capturing new ones with the camera. Once an image is selected, the user can send it to analysis. By doing it, a request to the API endpoint for shape analysis using the selected image is sent.

**Image Selection**:

- The user can choose to select an image from the gallery or capture a new one using the device's camera. This functionality is implemented using the ImagePicker package, which allows the application to interact with the device's image picker interface.

- Upon selecting an image, the file path of the selected image is obtained, and the image is displayed on the screen for the user to review.

**Sending Requests to API Endpoint**:

- When the user clicks the "Analyser les formes" button, the application sends a POST request to the API endpoint (https://noble-ambition-production.up.railway.app/upload/) using the http package.

- The selected image file is included as a multipart form data in the request payload using the http.MultipartFile.fromPath() method.

- Once the request is constructed, it is sent asynchronously using the send() method of the http.MultipartRequest object. The response from the server is then processed to obtain the analyzed image.

**Handling Responses**:

- Upon receiving the response from the server, the application processes the image data and updates the displayed image with the annotated shapes. The response contains the processed image data, which is decoded and displayed in the application interface.

- Additionally, the processed image is stored locally on the device for future reference using the getApplicationDocumentsDirectory() method from the path_provider package.

The code for the Flutter application is in this GitLab repository. The important file is in /my_app/lib/main.dart. That's where you'll find the code. To run the app you'll have to connect an android phone in developer mode to your computer or use an emulator in your IDE. When you have that selected, just run the code with F5 from the file 'main.dart'.

## Conclusion

During this project we discovered a lot about the computer vision field, it gave us a good entry point to further research and experiment with this technology that both of us are very interested in. It also clearly showed us how much we still have to discover and understand about AI and machine learning.

While we have a working app connected to the python script that does the required analysis, it doesn't fully work as intended. If the image only contains one shape, the output in the application is correct but, in the case of an image with several shapes, it displays the dozens of labels incorrectly.

We spent a lot of time trying to correct the code or finding where the problem could come from. In the end we suppose that the issue lies somewhere in the Flutter application because when launching the Python code locally in our machine instead of accessing it through Flutter we get the desired result.



While we are disappointed with the fact that it doesn't fully work in our Proof of Concept, we have several clues to follow and in the near future do more research to try to better understand what didn't work and improve this project.
