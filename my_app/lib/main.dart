import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomeScreen(),
    );
  }
}

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  String selectedImagePath = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellow.shade800,
      body: Center(
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                selectedImagePath == ''
                    ? Image.asset(
                        'assets/images/image_placeholder.png',
                        height: 450,
                        width: 350,
                        fit: BoxFit.contain,
                      )
                    : Image.file(
                        File(selectedImagePath),
                        height: 450,
                        width: 350,
                        fit: BoxFit.contain,
                      ),
                SizedBox(
                  height: 20.0,
                ),
                ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.green),
                        padding:
                            MaterialStateProperty.all(const EdgeInsets.all(20)),
                        textStyle: MaterialStateProperty.all(const TextStyle(
                            fontSize: 14, color: Colors.white))),
                    onPressed: () async {
                      print('okokok');
                      selectImage();
                      setState(() {});
                    },
                    child: const Text('Sélectionner image')),
                const SizedBox(height: 50.0),
                ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.green),
                        padding:
                            MaterialStateProperty.all(const EdgeInsets.all(20)),
                        textStyle: MaterialStateProperty.all(const TextStyle(
                            fontSize: 14, color: Colors.white))),
                    onPressed: () async {
                      print('okokok');
                      try {
                        await uploadImage(selectedImagePath);
                      } catch (e) {
                        print("Error uploading image: $e");
                      }
                      setState(() {});
                    },
                    child: const Text('Analyser les formes')),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> selectImage() async {
    String? imagePath = await selectImageFromDialog();

    if (imagePath != null && imagePath.isNotEmpty) {
      selectedImagePath = imagePath;
    } else {
      print("No image selected");
    }

    setState(() {});
  }

  Future<String?> selectImageFromDialog() async {
    return await showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          child: SingleChildScrollView(
            child: Container(
              height: 150,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  children: [
                    Text(
                      'Select Image From !',
                      style: TextStyle(
                          fontSize: 18.0, fontWeight: FontWeight.bold),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        TextButton(
                          onPressed: () async {
                            String? selectedImagePath =
                                await selectImageFromGallery();
                            Navigator.pop(context, selectedImagePath);
                          },
                          child: Column(
                            children: [
                              Image.asset(
                                'assets/images/gallery.png',
                                height: 60,
                                width: 60,
                              ),
                              Text('Gallery'),
                            ],
                          ),
                        ),
                        TextButton(
                          onPressed: () async {
                            String? selectedImagePath =
                                await selectImageFromCamera();
                            Navigator.pop(context, selectedImagePath);
                          },
                          child: Column(
                            children: [
                              Image.asset(
                                'assets/images/camera.png',
                                height: 60,
                                width: 60,
                              ),
                              Text('Camera'),
                            ],
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Future<String?> selectImageFromGallery() async {
    XFile? file = await ImagePicker()
        .pickImage(source: ImageSource.gallery, imageQuality: 80);
    return file?.path;
  }

  Future<String?> selectImageFromCamera() async {
    XFile? file = await ImagePicker()
        .pickImage(source: ImageSource.camera, imageQuality: 80);
    return file?.path;
  }

  Future<String?> Test() async {
    print('testtesttest');
  }

  Future<void> uploadImage(String filePath) async {
    print('Image uploaded');

    var url = "https://noble-ambition-production.up.railway.app/upload/";

    var request = http.MultipartRequest('POST', Uri.parse(url));

    request.files.add(await http.MultipartFile.fromPath('file', filePath));
    print('d');
    var streamedResponse = await request.send();
    var response = await http.Response.fromStream(streamedResponse);
    print('fonctionné?');
    final Directory appDirectory = await getApplicationDocumentsDirectory();
    final String imagePath = '${appDirectory.path}/image.jpg';
    File(imagePath).writeAsBytesSync(response.bodyBytes);

    setState(() {
      // Mettre à jour le chemin de l'image
      selectedImagePath = imagePath;
    });
    print('YdfsdfEAHH');
  }
}
